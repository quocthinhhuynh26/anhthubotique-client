# AnhthuBotique-Client

Giao diện cho website AnhThuBoutique.

## Khởi tạo file tsconfig.json

`RUN: tsc --init`

- Module: Lựa chọn định dạng .js được complied từ .ts. Ví dụ: Nếu module = es6 thì complier sẽ giả định là javascript engine sẽ execute được code được viết theo định dạng es6 (import & export). Nếu module = es5 thì sẽ là (require & module.export)

- Target: Lựa chọn phiên bản javascript để chạy complied-code (được complied từ phiên bản module). Nếu target = es5 thì các browser có hỗ trợ javascript es5 sẽ chạy được complied-code. Còn nếu targer = es6 thì complied-code sẽ không chạy được trên trên các browser hoặc là engine chưa được hỗ trợ execute phiên bản javascript es6 (Ví dụ như node v8)

## Cập nhật thay đổi cho cơ sở dữ liệu

`npx prisma migrate dev --name 'Tên file'`

## Deploy

`npx prisma migrate deploy`

## Sau khi thay đổi file schema.prisma thì chạy lại Prisma Client

`npx prisma generate`

## Prisma DateTime for Postgres (DateTime Types Related)

[Link](https://www.prisma.io/docs/reference/api-reference/prisma-schema-reference#postgresql-6)

-------- CẤU HÌNH SERVER -------

## Declare biến global.Botique

[Link](https://stackoverflow.com/questions/51610583/ts-node-ignores-d-ts-files-while-tsc-successfully-compiles-the-project)

1. Actual Code: ./config
