import { ResponseContent } from "../interface";
import { ValidationError } from "joi";
const success = (
  data: any,
  message: string = "successfully called api",
  httpCode: number = 200,
  code: number = 200
): ResponseContent => {
  return {
    httpCode,
    code,
    message,
    data,
    error: false,
  };
};

const error = (
  message: string,
  httpCode: number = 400,
  code: number = 400
): ResponseContent => {
  return {
    httpCode,
    code,
    error: true,
    message,
    data: null,
  };
};

const validationError = (message: ValidationError): ResponseContent => {
  return {
    httpCode: 400,
    code: 400,
    error: true,
    message,
    data: null,
  };
};

const duplicationFound = () => {
  return {
    httpCode: 400,
    code: 401,
    error: true,
    message: "Dữ liệu đã tồn tại",
    data: null,
  };
};

export default {
  success,
  error,
  validationError,
  duplicationFound,
};
