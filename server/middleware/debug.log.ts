import { Request, Response, NextFunction } from "express";
import moment from "moment";
import { Prisma } from "@prisma/client";
import { ResponseContent } from "../interface";

const logRequest = async (req: Request, res: Response, next: NextFunction) => {
  const ipAddress: string = req.headers["x-forwarded-for"]
    ? req.headers["x-forwarded-for"].toString()
    : "";

  const bodyContent: Object = req.method === "GET" ? req.query : req.body;

  req.start = moment();

  const requestLogging: Prisma.ServerDebugLogCreateInput = {
    name: "Request Logging",
    http_method: req.method,
    uri_accessed: req.originalUrl,
    ip_address: ipAddress,
    request_content: JSON.stringify(bodyContent),
    request_time: moment().toDate(),
    username: "",
  };

  await Botique.serverDebugLog.create({
    data: requestLogging,
  });

  next();
};
const logResponse = async (
  req: Request,
  responseContent: ResponseContent
): Promise<void> => {
  const ipAddress: string = req.headers["x-forwarded-for"]
    ? req.headers["x-forwarded-for"].toString()
    : "";

  const processTime: number = moment
    .duration(moment().diff(req.start))
    .asSeconds();

  const responseLogging: Prisma.ServerDebugLogCreateInput = {
    name: "Response Logging",
    http_method: req.method,
    uri_accessed: req.originalUrl,
    ip_address: ipAddress,
    response_content: JSON.stringify(responseContent),
    http_code: responseContent.httpCode,
    status_code: responseContent.code,
    username: " ",
    response_time: moment().toDate(),
    process_time: processTime,
  };

  await Botique.serverDebugLog.create({ data: responseLogging });
};

export { logRequest, logResponse };

// "dev": "nodemon  --files app.ts ./config/global.d.ts"
