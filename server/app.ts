import express, { Application, Request, Response, NextFunction } from "express";
import("./config/config.global");
import { ResponseContent } from "./interface";

import { logRequest, logResponse } from "./middleware/debug.log";
const app: Application = express();

import morgan from "morgan";
import bodyParser from "body-parser";
import cors from "cors";

app.use(morgan("dev"));
app.use(bodyParser.json({ limit: "100mb" }));
app.use(
  bodyParser.urlencoded({
    limit: "100mb",
    extended: false,
  })
);
//open cors
app.use(
  cors({
    origin: "*",
    methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
    preflightContinue: false,
    optionsSuccessStatus: 204,
  })
);

app.use(logRequest);

app.get("/", (req: Request, res: Response, next: NextFunction) => {
  res.status(200).send("Welcome to Anh Thu Botique Apis");
});

import product from "./modules/product/product.routes";
import size from "./modules/size/size.routes";

app.use("/product", product);
app.use("/size", size);

//Global Error Handler
app.use(async (err: any, req: Request, res: Response, next: NextFunction) => {
  const content: ResponseContent = {
    httpCode: err.httpCode ? err.httpCode : 400,
    code: err.code ? err.code : 400,
    error: err.error ? true : false,
    message: err.message ? err.message : "Oops, Unexpected Error Happened",
    data: err.data ? err.data : null,
  };

  await logResponse(req, content);
  res.status(content.httpCode).send(content);
});

app.listen(process.env.PORT || 2999, () => {
  console.log(
    `Server is running on port ${process.env.PORT} - VERSION: ${process.env.VERSION}`
  );
});
