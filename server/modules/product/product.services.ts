import { Prisma } from "@prisma/client";
import moment from "moment";
const getList = () => Botique.product.findMany();

interface productCreation {
  name: string;
  barcode: string;
  created_date: string;
  supplier?: number;
}

const create = async (queries: productCreation) => {
  const creation: Prisma.ProductCreateInput = {
    name: queries.name,
    barcode: queries.barcode,
    created_date: moment(queries.created_date, "YYYY-MM-DD").toDate(),
  };
  if (queries.supplier) creation.supplier = queries.supplier;

  return await Botique.product.create({ data: creation });
};
export default { getList, create };
