import { Request, Response, NextFunction } from "express";
import populateResponse from "../../util/populate_response";
import productServices from "./product.services";
import Joi from "joi";

const getList = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const list = await productServices.getList();
    next(populateResponse.success(list));
  } catch (e) {
    next(e);
  }
};

const create = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const schema = Joi.object({
      name: Joi.string().required(),
      barcode: Joi.string().required(),
      created_date: Joi.string().required(),
      supplier: Joi.number().optional(),
    }).validate(req.body);

    if (schema.error)
      return next(populateResponse.validationError(schema.error));

    const creation = await productServices.create(schema.value);
    next(populateResponse.success(creation));
  } catch (e) {
    next(e);
  }
};

export default { getList, create };
