import express, { Router, Request, Response, NextFunction } from "express";
import productController from "./product.controllers";

const router: Router = express.Router();

router.get("/list", productController.getList);

router.post("/create", productController.create);

export default router;
