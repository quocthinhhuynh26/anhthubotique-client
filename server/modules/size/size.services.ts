import { Prisma } from "@prisma/client";
import moment from "moment";
const getList = () => Botique.size.findMany();

interface sizeCreation {
  name: string;
}

const create = (queries: sizeCreation) => {
  const creation: Prisma.SizeCreateInput = {
    name: queries.name,
  };

  return Botique.size.create({ data: creation });
};

const findFirst = (sizeName: string): Promise<any[]> => {
  return Botique.$queryRaw`
    SELECT name FROM size WHERE LOWER(name) = LOWER(${sizeName});
  `;
};

export default { getList, create, findFirst };
