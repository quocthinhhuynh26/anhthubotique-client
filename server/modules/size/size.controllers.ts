import { Request, Response, NextFunction } from "express";
import populateResponse from "../../util/populate_response";
import sizeServices from "./size.services";
import Joi from "joi";

const getList = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const list = await sizeServices.getList();
    next(populateResponse.success(list));
  } catch (e) {
    next(e);
  }
};

const create = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const schema = Joi.object({
      name: Joi.string().required(),
    }).validate(req.body);

    if (schema.error)
      return next(populateResponse.validationError(schema.error));

    const checker = await sizeServices.findFirst(schema.value.name);

    if (checker.length !== 0) return next(populateResponse.duplicationFound());

    const creation = await sizeServices.create(schema.value);
    next(populateResponse.success(creation));
  } catch (e: any) {
    next(e);
  }
};

export default { getList, create };
