import express, { Router, Request, Response, NextFunction } from "express";
import sizController from "./size.controllers";

const router: Router = express.Router();

router.get("/list", sizController.getList);

router.post("/create", sizController.create);

export default router;
