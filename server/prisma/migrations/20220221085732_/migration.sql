/*
  Warnings:

  - You are about to drop the column `image_fileId` on the `account` table. All the data in the column will be lost.
  - Added the required column `barcode` to the `product_size_price` table without a default value. This is not possible if the table is not empty.
  - Added the required column `name` to the `product_size_price` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "account" DROP COLUMN "image_fileId",
ADD COLUMN     "image_file_id" TEXT;

-- AlterTable
ALTER TABLE "product_size_price" ADD COLUMN     "barcode" TEXT NOT NULL,
ADD COLUMN     "name" TEXT NOT NULL;
