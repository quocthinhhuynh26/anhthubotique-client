/*
  Warnings:

  - You are about to drop the column `price` on the `product_size_price` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "account" ADD COLUMN     "image_download_url" TEXT,
ADD COLUMN     "image_fileId" TEXT,
ADD COLUMN     "image_url" TEXT;

-- AlterTable
ALTER TABLE "product_size_price" DROP COLUMN "price",
ADD COLUMN     "gia_ban_si_lon" INTEGER NOT NULL DEFAULT 0,
ADD COLUMN     "gia_ban_si_nho" INTEGER NOT NULL DEFAULT 0,
ADD COLUMN     "gia_goc" INTEGER NOT NULL DEFAULT 0;
