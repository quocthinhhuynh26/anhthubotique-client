import { Moment } from "moment";

declare global {
  namespace Express {
    interface Request {
      start: Moment;
    }
  }
}
