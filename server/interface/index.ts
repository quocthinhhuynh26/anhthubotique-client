import { ValidationError } from "joi";
interface ResponseContent {
  httpCode: number;
  code: number;
  error: boolean;
  message?: string | ValidationError;
  data?: any;
}

export { ResponseContent };
